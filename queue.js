let collection = [];

// Write the queue functions below.

// print queue
function print(){
    return collection
}

//  Enqueue a new element.
function enqueue(element){   
    collection.push(element);
    return collection;      
}
// dequeue first element
function dequeue(element){
    collection.shift(element);
    return collection;
}

// check first element

function front(){
    return collection[0];
}

// get queue size
function size(){
    return collection.length;
}

// check if queue is empty
function isEmpty(){
    if( collection.length === 0){
        return true
    } else {
        return false
    }
}

module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty

};

